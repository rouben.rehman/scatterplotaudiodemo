const ncp = require('ncp').ncp;
const path = require('path');

const source = path.join(__dirname, 'dist');
const destination = path.join(__dirname, 'public');

ncp(source, destination, function (err) {
  if (err) {
    return console.error(err);
  }
  console.log('Build files copied to public folder!');
});
